//TMDB https://api.themoviedb.org/3/movie/76341
const zlib = require("zlib");
const PouchDB = require("pouchdb");
const db = new PouchDB('cache');
const _ = require("lodash");

function keysToCamel(obj) {
    if (_.isObject(obj)) {
        let n = {};
        Object.keys(obj).forEach(k => (n[_.camelCase(k)] = keysToCamel(obj[k])));
        return n;
    } else if (_.isArray(obj)) {
        obj.map(i => keysToCamel(i))
    }

    return obj;
}

const proxyOptions = {
    target: "https://api.themoviedb.org", // target host
    changeOrigin: true,
    pathRewrite: function (path, req) {
        return path.replace('/tmdb', '')
    },
    onProxyReq: function onProxyReq(proxyReq, req, res) {
        //Authentication
        proxyReq.setHeader('Authorization', `Bearer ` + process.env.TMDB_API_KEY)
        // proxyReq.setHeader('accept-encoding', `gzip;q=0,deflate,sdch`)
    },
    onProxyRes: function onClose(proxyRes, req, res) {
        var body = [];
        proxyRes.on('data', function (chunk) {
            body.push(chunk);
        });
        proxyRes.on('end', async function () {
            let buffer = Buffer.concat(body);

            try {
                let isGzip = proxyRes.headers['content-encoding'] === 'gzip';
                let isBrotli = proxyRes.headers['content-encoding'] === 'br';
                body = isGzip
                    ? zlib.gunzipSync(buffer).toString('utf8')
                    : isBrotli
                        ? zlib.brotliDecompressSync(buffer).toString('utf8')
                        : buffer.toString();
            } catch (e) {
                body = buffer.toString();
            }
            // Convert to camel case
            // body = keysToCamel(JSON.parse(body))
            try {
                body = JSON.parse(body)
            } catch (e) {
            }
            //Insert into database
            let key = req.url
            try {
                let doc = await db.get(key)
                db.put({
                    body,
                    status: proxyRes.statusCode,
                    time: new Date(),
                    _id: key,
                    _rev: doc._rev
                });
            } catch (err) {
                db.put({
                    body,
                    status: proxyRes.statusCode,
                    time: new Date(),
                    _id: key
                });
            }
            res.json(body).end();
        });
    },
    selfHandleResponse: true,
    logLevel: 'debug'
}

module.exports = {
    proxyOptions
}
