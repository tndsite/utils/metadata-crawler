const PouchDB = require("pouchdb");

const db = new PouchDB('cache');

//Cache handler
const cache = async (req, res, next) => {
    let key = req.url
    try {
        let doc = await db.get(key)
        let differenceInSeconds = ((new Date().getTime()) - Date.parse(doc.time)) / 1000;
        //24 hours
        if (differenceInSeconds > 86400) {
            next()
        } else {
            let result;
            try {
                result = JSON.parse(doc.body);
            } catch (e) {
                result = doc.body;
            }
            res.status(doc.status).json(result)
        }
    } catch (e) {
        console.error("Error while cacheing ", e)
        next()
    }
}


module.exports = {
    cache
}
