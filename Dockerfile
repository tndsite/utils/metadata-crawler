FROM node:latest

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install node-pre-gyp -g
RUN npm install
COPY . .
RUN npm run swagger-autogen


ENV TMDB_API_KEY=API_KEY_HERE

EXPOSE 3000
CMD [ "npm", "run", "start" ]
