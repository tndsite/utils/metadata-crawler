const express = require('express');
const path = require('path');
const compression = require('compression');
require('dotenv').config();
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const timeout = require('connect-timeout')
const cors = require('cors');
const indexRouter = require('./routes/index');
const rfs = require("rotating-file-stream");
const {createProxyMiddleware} = require('http-proxy-middleware');
const zlib = require("zlib");
const {cache} = require("./middleware/cache");
const {proxyOptions} = require("./middleware/tmdbProxy");


const pad = num => (num > 9 ? "" : "0") + num;
const generator = (time, index) => {
    if (!time) return "request.log";

    let month = time.getFullYear() + "" + pad(time.getMonth() + 1);
    let day = pad(time.getDate());

    return `${month}/${day}-file-${index}.log`;
};
const stream = rfs.createStream(generator, {
    size: "20M", // rotate every 10 MegaBytes written
    compress: "gzip", // compress rotated files
    maxFiles: 10
});

const app = express();
logger.token('bearer', function (req, res) {
    return req.headers.authorization || "No auth";
})
logger.token('body', function (req, res) {
    return JSON.stringify(req.body);
})

app.use(logger('[:date[clf]] :response-time ms :remote-addr ":method :url" :status ":user-agent" :bearer', {
    stream
}));
app.use(logger('[:date[clf]] :response-time ms :remote-addr ":method :url" :status ":user-agent" :bearer'));
//Todo solo se necessario
// app.use(logger('[:date[clf]] :response-time ms :remote-addr ":method :url" :status ":user-agent" :body'));


app.use(compression())
app.use(timeout('10s'))
app.use(cors());
app.use(express.json());
// app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);


app.use("/tmdb", cache)
app.use("/tmdb", createProxyMiddleware(proxyOptions))


// Handling Errors
app.use((err, req, res, next) => {
    console.log("Error is error: ", err);
    err.statusCode = err.statusCode || 500;
    err.message = err.message || "Internal Server Error";
    res.status(500).json({
        error: true,
        message: err.message,
    });
});
module.exports = app;
