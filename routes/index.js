const {exec, spawn} = require("child_process");
const express = require('express');
// const {handleRes} = require("./utility");
const router = express.Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./../swagger-output.json');
const packageJson = require('./../package.json');
router.use('/api-docs', swaggerUi.serve);


router.get('/api-docs', (req, res) => {
  let swagParsed=swaggerDocument
  swagParsed.info.version = packageJson.version
  swagParsed.host = "metadata-crawler.quix.cf"
  swagParsed.schemes = ["https"]
  let swDocTemp = swaggerUi.setup(swagParsed)
  swDocTemp(req,res)
});
module.exports = router;
